#!/bin/bash

declare -a kernels
kernels+=("vmlinuz-5.14.0-rt21-100-+")
kernels+=("vmlinuz-5.14.0-rt21-255-+")
kernels+=("vmlinuz-5.14.0-rt21-1000-+")
kernels+=("vmlinuz-5.14.0-rt21-10000-+")
kernels+=("vmlinuz-5.14.0-rt21-50000-+")

declare -A kernel_opts
for kernel in "${kernels[@]}"; do
    kernel_opts[$kernel]="$(echo $kernel | sed 's/.*rt21-\([0-9]\+\)-+/\1/')"
done

declare -a sample_interval_ms
sample_interval_ms+=(1)
sample_interval_ms+=(2)
sample_interval_ms+=(5)
sample_interval_ms+=(10)
sample_interval_ms+=(20)
sample_interval_ms+=(50)
sample_interval_ms+=(100)
sample_interval_ms+=(200)

declare -a test_functions
for fct in $(sed '/^ *FCT_LIST_ADD/!d;s/.*(\(.*\)).*/\1/' memsan_test.c); do
    test_functions+=($fct)
done

dir="memsan_results"
repo_kernel="https://gitlab.com/CentOS/automotive/src/kernel/kernel-automotive-9.git"
repo_test="https://gitlab.com/dbrendel/trigger-memory-sanitizer.git"

ssh_args=""

gen_ssh_key() {
    local keyfile

    keyfile="$(mktemp ${dir:?}/test_kernels_ssh_key.XXXXXX)"
    chmod 600 $keyfile
    ssh-keygen -q -N "" -t ed25519 -f $keyfile >/dev/null <<< 'y'

    echo "$keyfile"
}

create_instance() {
    local name="$1"
    local ssh_key="$2"
    local opts

    opts="--no-graphic"
    opts+=" --name $name"
    opts+=" --vcpus $(nproc)"
    opts+=" --disksize 40"
    opts+=" --ssh_path $ssh_key.pub"
    opts+=" --timeout 0"

    # This has some ugly output, not meant for parsing
    testcloud instance create $opts centos-stream:9 >/dev/null 2>&1

    return $?
}

get_instance_ip() {
    local name="$1"
    local ip

    ip=$(testcloud instance list | grep -w $name | awk '{ print $2 }')
    echo $ip
}

get_instance_port() {
    local name="$1"
    local port

    port=$(testcloud instance list | grep -w $name | awk '{ print $3 }')
    echo $port
}

seconds_elapsed() {
    local start="$1"
    local elapsed
    local current

    current=$(date +%s)
    elapsed=$((current - start))

    echo $elapsed
}

wait_for_instance() {
    local timeout=30
    local ip
    local port
    local start

    test -n "$3" && timeout="$3"

    ip="$1"
    port="$2"

    start=$(date +%s)
    while test $(seconds_elapsed $start) -lt $timeout; do
        nc --wait 1 -z $ip $port && return 0
    done

    return 1
}

cleanup() {
    test -d ${dir:?}/$(current_kernel) && rm -r ${dir:?}/$(current_kernel)
    exit
}

trap cleanup INT

current_kernel() {
    echo "vmlinuz-$(ssh $ssh_args uname -r)"
}

kernel_in_list() {
    local kernel="$1"

    for k in "${kernels[@]}"; do
        if test "$kernel" = "$k"; then
            return 0
        fi
    done

    return 1
}

has_kernel() {
    local kernel="$1"

    if ssh $ssh_args test -f /boot/$kernel; then
        return 0
    else
        return 1
    fi
}

next_kernel() {
    for kernel in "${kernels[@]}"; do
        if test -d $dir/$kernel; then
            continue
        fi

        echo "$kernel"
        return
    done
}

build_kernel() {
    local kernel_dir="$1"
    local opts="$2"

    build_target $kernel_dir dist-configs
    ssh $ssh_args cp $kernel_dir/redhat/configs/kernel-automotive-5.14.0-$(uname -m).config \
                     $kernel_dir/.config

    update_kernel_conf $kernel_dir "CONFIG_KFENCE_NUM_OBJECTS" "$opts"
    update_kernel_conf $kernel_dir "CONFIG_LOCALVERSION" "\\\"-$opts-\\\""

    build_target $kernel_dir ""
    build_target $kernel_dir modules
    build_target $kernel_dir modules_install
    build_target $kernel_dir install
}

has_test_data() {
    local kernel="$1"
    local interval="$2"
    local function="$3"

    if ! test -f $dir/$kernel/$function/dmesg_$interval\_01.log; then
        return 1
    fi

    return 0
}

init_memsan() {
    local cmdline
    local kfence
    local kasan

    cmdline="$(dmesg | grep "Command line:")"
    kfence="$(dmesg | grep -i kfence)"
    kasan="$(dmesg | grep -i kasan)"

    #TODO!
}

usage() {
    echo "$0 <user> <ip> <port>"
    echo " - OR - "
    echo "$0 <testcloud instance name>"
}

cmdline_to() {
    local instance_name
    local ipaddr
    local port
    local user
    local ssh_key
    local ssh_target

    if test $# -eq 2; then
        instance_name="$2"
        user=cloud-user
        ipaddr=$(get_instance_ip $instance_name)
        port=$(get_instance_port $instance_name)
    elif test $# -eq 4; then
        user="$2"
        ipaddr="$3"
        port="$4"
    else
        usage
        exit 0
    fi

    case $1 in
        "user")
            echo $user
            ;;
        "port")
            echo $port
            ;;
        "ipaddr")
            echo $ipaddr
            ;;
    esac
}

is_valid_repo() {
    local repo="$1"
    local dir="$(basename --suffix=.git $repo)"
    local remote

    ssh $ssh_args test -d $dir/.git
    if test $? -ne 0; then
        return 1
    fi

    remote="$(ssh -t $ssh_args "cd $dir/; git remote --verbose" \
             | grep "fetch" | awk '{ print $2 }')"

    if ! test "$remote" = "$repo"; then
        return 2
    fi

    return 0
}

create_repo() {
    local repo="$1"
    local dir="$(basename --suffix=.git $repo)"

    # Use shallow clone because otherwise we get that error:
    # fatal: fetch-pack: invalid index-pack output
    # Additionally we do not require more than the latest state
    ssh $ssh_args git clone --depth 1 $repo $dir
}

build_target() {
    local dir="$1"
    local target="$2"
    local njobs
    local privileged

    # I don't like it, this needs a better solution. Mayber another argument?
    if test -n "$(echo $target | grep install)"; then
        privileged="sudo"
    fi

    njobs=$(ssh $ssh_args nproc)
    ssh $ssh_args $privileged make --jobs $njobs -C $dir $target >/dev/null 2>&1
}

update_kernel_conf() {
    local dir="$1"
    local key="$2"
    local val="$3"

    ssh $ssh_args sed -i \"s/\\\($key=\\\).*/\\1$val/\" $dir/.config
}

install_pkgs() {
    local pkgs="$1"
    local pkg_q

    for pkg in $pkgs; do
        if ! ssh $ssh_args rpm --quiet --query $pkg; then
            pkg_q+="$pkg "
        fi
    done

    if test -n "$pkg_q"; then
        ssh $ssh_args sudo dnf -y install $pkg_q
    fi
}

get_arr_idx() {
    declare -n arr="$1"
    local idx="$2"

    for i in "${!arr[@]}"; do
        if test "$idx" = "${arr[$i]}"; then
            echo $i
            return 0
        fi
    done
}

boot_kernel() {
    local kernel="$1"
    local interval=${sample_interval_ms[0]}
    test -n "$2" && local interval="$2"

    ssh $ssh_args sudo grubby --set-default=/boot/$kernel >/dev/null 2>&1
    if test $? -ne 0; then
        echo "Failed setting a new kernel!"
        exit 1
    fi

    ssh $ssh_args sudo grubby --update-kernel=ALL --args=kfence.sample_interval=$interval >/dev/null 2>&1
    if test $? -ne 0; then
        echo "Failed setting a new sample interval!"
        exit 1
    fi

    ssh $ssh_args sudo reboot

    sleep 15
}

main() {
    local ssh_opts
    local ssh_target
    local kernel
    local user
    local ipaddr
    local port
    local proceed
    local kernel_dir
    local test_dir
    local next_interval

    if ! test -d $dir; then
        mkdir $dir
    fi

    user=$(cmdline_to user "${@}")
    port=$(cmdline_to port "${@}")
    ipaddr=$(cmdline_to ipaddr "${@}")

    kernel_dir="$(basename --suffix=.git $repo_kernel)"
    test_dir="$(basename --suffix=.git $repo_test)"

    ssh_target="-p $port $user@$ipaddr"

    test -z "$ssh_key" && ssh_key=$(gen_ssh_key)
    ssh_opts="-o StrictHostKeyChecking=no"
    ssh_opts+=" -o ControlMaster=auto"
    ssh_opts+=" -o ControlPath=$dir/%r@%h:%p"
    ssh_opts+=" -o ControlPersist=10m"
    ssh_opts+=" -i $ssh_key"

    ssh_args="$ssh_opts $ssh_target"

    if ! wait_for_instance $ipaddr $port; then
        echo "Failed to detect listener on port $port at $ipaddr"
        exit 1
    fi

    if ! ssh -q -o BatchMode=yes $ssh_args exit; then
        echo "Password-less login did not work. Copying pubkey.."
        ssh-copy-id $ssh_args >/dev/null 2>&1
    fi

    install_pkgs "gcc bc diffutils elfutils-libelf-devel bison flex openssl-devel
                  procps-ng util-linux-core git rpm-build python3-devel dwarves"

    for repo in $repo_kernel $repo_test; do
        if ! is_valid_repo $repo; then
            create_repo $repo || exit 1
        fi
    done

    for kernel in "${kernels[@]}"; do
        if ! has_kernel $kernel; then
            echo "$kernel not found! Building.."
            build_kernel "$kernel_dir" ${kernel_opts[$kernel]}
        fi
    done

    kernel=$(current_kernel)
    if ! kernel_in_list $kernel; then
        echo "Current kernel not scheduled to be tested."
        kernel="$(next_kernel)"
        if test -z "$kernel"; then
            echo "All kernels tested!"
            exit
        fi

        echo "Booting kernel $kernel.."
        boot_kernel $kernel

        main "${@}"
    fi

    for interval in "${sample_interval_ms[@]}"; do
        if has_test_data $kernel $interval $function; then
            echo "Found test data for kernel $(current_kernel) with sampling interval ${interval}ms testing $function. Skipping.."
            continue
        fi
        for function in "${test_functions[@]}"; do
            if has_test_data $kernel $interval $function; then
                echo "Found test data for kernel $(current_kernel) with sampling interval ${interval}ms testing $function. Skipping.."
                continue
            fi

            for run in $(seq --equal-width 1 20); do
                echo "Test run $run of $function for kernel $(current_kernel) with sampling interval ${interval}ms.."

                if test $run -eq 1; then
                    mkdir --parents $dir/$kernel/$function
                    build_target $test_dir clean
                    build_target $test_dir
                fi
                ssh $ssh_args sudo dmesg --clear
                ssh $ssh_args sudo insmod $test_dir/memsan_test.ko num_runs=1000000 tests=$function
                ssh $ssh_args sudo rmmod memsan_test
                ssh $ssh_args dmesg -t > $dir/$kernel/$function/dmesg_$interval\_$run.log
            done
        done

        if ! test $interval -eq "${sample_interval_ms[-1]}"; then
            next_interval="${sample_interval_ms[$(( $(get_arr_idx sample_interval_ms $interval) + 1)) ]}"

            echo "Booting kernel $kernel with interval ${next_interval}ms.."
            boot_kernel $kernel $next_interval

            main "${@}"
        fi

    done

    kernel="$(next_kernel)"
    if test -z "$kernel"; then
        echo "All kernels tested!"
        exit
    fi

    echo "Booting kernel $kernel.."
    boot_kernel $kernel

    main "${@}"
}

main "${@}"

