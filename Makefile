PWD = $(shell pwd)
KVER ?= $(shell uname -r)
KDIR := /lib/modules/$(KVER)/build

SHELLCHECKOPTS := --severity=warning --exclude=SC2046

obj-m := memsan_test.o

.PHONY: clean

all:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

pre:
	$(MAKE) -C $(KDIR) M=$(PWD) modules $(obj-m:.o=.i)

shellcheck:
	-shellcheck $(SHELLCHECKOPTS) run-tests.sh
	-shellcheck $(SHELLCHECKOPTS) parse-logs.sh

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean

